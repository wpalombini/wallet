import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { SettingsComponent } from './pages/settings/settings.component';

const routes: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'settings', component: SettingsComponent },
    { path: '', component: HomeComponent },
    { path: '**', redirectTo: '/', pathMatch: 'full' },
];

const options: ExtraOptions = {
    // enableTracing: !environment.production,
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'top',
};
@NgModule({
    imports: [RouterModule.forRoot(routes, options)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
