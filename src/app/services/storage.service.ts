import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class StorageService {
    public static readonly isSigningUp: string = 'is-signing-up';
    public static readonly linkSanctuary: string = 'link-sanctuary';
    public static readonly returnUrl: string = 'return-url';

    constructor() {}

    public getFromSession<T>(storageName: string): T {
        return JSON.parse(sessionStorage.getItem(storageName)) as T;
    }

    public saveToSession<T>(storageName: string, data: T): void {
        this.deleteFromSession(storageName);

        sessionStorage.setItem(storageName, JSON.stringify(data));
    }

    public deleteFromSession(storageName: string): void {
        sessionStorage.removeItem(storageName);
    }
}
