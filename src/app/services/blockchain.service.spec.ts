import { TestBed } from '@angular/core/testing';

import { BlockchainService } from './blockchain.service';

describe('BlockchainService', (): void => {
    let service: BlockchainService;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(BlockchainService);
    });

    it('should be created', (): void => {
        expect(service).toBeTruthy();
    });
});
