import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root',
})
export class ResultMessageService {
    constructor(private snackBar: MatSnackBar) {}

    public sendTextSuccessNotification(): void {
        this.snackBar.open('Successfully saved!', 'Dismiss', {
            duration: 10000,
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
        });
    }
}
