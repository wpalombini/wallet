import { Injectable } from '@angular/core';
import { from, Observable, Subject } from 'rxjs';
import WEB3 from 'web3';

@Injectable({
    providedIn: 'root'
})
export class BlockchainService {

    private web3: WEB3;

    private balanceSubject: Subject<string> = new Subject();

    constructor() {
        this.web3 = new WEB3('http://192.168.1.50:7545');
    }

    public getAccountList(): Observable<Array<string>> {
        return from(this.web3.eth.getAccounts());
    }

    public getBalanceForAddress(address: string): Observable<string> {

        this.web3.eth.getBalance(address)
            .then((balance: string): void => {
                this.balanceSubject.next(this.web3.utils.fromWei(balance));
            });

        return this.balanceSubject.asObservable();
    }

    public send(fromAddress: string, toAddress: string, value: number): void {
        this.web3.eth.sendTransaction({
            from: fromAddress,
            to: toAddress,
            value: this.web3.utils.toWei(value.toString())
        })
            .then((): void => {
                this.web3.eth.getBalance(fromAddress)
                    .then((balance: string): void => {
                        this.balanceSubject.next(this.web3.utils.fromWei(balance));
                    });
            });
    }
}
