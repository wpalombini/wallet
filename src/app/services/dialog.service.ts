import { Injectable, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class DialogService {
    private dialogWidth: string = '500px';

    constructor(private dialog: MatDialog) { }

    public openTemplateModal(template: TemplateRef<void>): MatDialogRef<void, any> {
        const templateModal: MatDialogRef<void, any> = this.dialog.open(template, {
            width: this.dialogWidth,
        });

        return templateModal;
    }

    public openConfirmationDialog(): MatDialogRef<ConfirmationDialogComponent, any> {
        const dialogRef: MatDialogRef<ConfirmationDialogComponent, any> = this.dialog.open(
            ConfirmationDialogComponent,
            {
                width: this.dialogWidth,
                data: { name: 'name data' },
            }
        );

        return dialogRef;
    }

    public getConfirmationDialogResult(): Observable<boolean> {
        return this.openConfirmationDialog()
            .afterClosed()
            .pipe(
                take(1),
                map((result: any): boolean => {
                    return result ?? false;
                })
            );
    }
}
