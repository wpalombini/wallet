import { TestBed } from '@angular/core/testing';

import { DialogService } from './dialog.service';

describe('DialogService', (): void => {
    let service: DialogService;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(DialogService);
    });

    it('should be created', (): void => {
        expect(service).toBeTruthy();
    });
});
