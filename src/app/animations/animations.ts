import {
    trigger,
    transition,
    style,
    query,
    group,
    animate,
    animateChild,
    AnimationTriggerMetadata,
} from '@angular/animations';

const optional: any = { optional: true };

export const slideInAnimation: AnimationTriggerMetadata = trigger('routeAnimations', [
    transition('* => *', [
        query(
            ':enter, :leave',
            [
                style({
                    position: 'absolute',
                    top: 30,
                    left: 0,
                    width: '100%',
                }),
            ],
            optional
        ),
        query(':enter', [style({ left: '-100%' })], optional),
        query(':leave', animateChild(), optional),
        group([
            query(':leave', [animate('300ms ease-out', style({ left: '100%' }))], optional),
            query(':enter', [animate('300ms ease-out', style({ left: '0%' }))], optional),
        ]),
        query(':enter', animateChild(), optional),
    ]),
]);
