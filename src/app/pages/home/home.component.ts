import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { BlockchainService } from 'src/app/services/blockchain.service';
import { DialogService } from 'src/app/services/dialog.service';
import * as QRCode from 'qrcode';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

    @ViewChild('chooseAddressTemplate', { static: true })
    public chooseAddressTemplate: TemplateRef<void>;

    @ViewChild('sendFormDirective')
    public sendFormDirective: NgForm;

    @ViewChild('receiveTemplate', { static: true })
    public receiveTemplate: TemplateRef<void>;

    @ViewChild('sendTemplate', { static: true })
    public sendTemplate: TemplateRef<void>;

    public accountList$: Observable<Array<string>>;
    public balance: string;
    public currentAddress: string;
    public sendForm: FormGroup;

    private subscriptions: Subscription = new Subscription();

    constructor(private blockchainService: BlockchainService, private dialogService: DialogService, private fb: FormBuilder) { }

    public ngOnInit(): void {
        this.setupForms();

        this.accountList$ = this.blockchainService.getAccountList().pipe(take(1));

        if (!this.currentAddress) {
            this.subscriptions.add(this.dialogService.openTemplateModal(this.chooseAddressTemplate)
                .afterClosed()
                .subscribe((result: string): void => {
                    if (result) {
                        this.currentAddress = result;
                        this.blockchainService.getBalanceForAddress(result)
                            .subscribe((balance: string): string => this.balance = balance);
                    }
                }));
        }

    }

    public ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
    }

    public onCameraClicked(event: Event): void {
        event.preventDefault(); // prevent validation to be triggered
    }

    public onSend(): void {
        this.subscriptions.add(this.dialogService.openTemplateModal(this.sendTemplate)
            .afterClosed()
            .subscribe((): void => this.sendFormDirective.resetForm()));


    }

    public send(): void {
        // this.blockchainService.send(this.currentAddress, '0x205f292740B2cA0B65A6D6a6ecA063dD667C5102', 2);
    }

    public showModalReceive(): void {
        this.subscriptions.add(this.dialogService.openTemplateModal(this.receiveTemplate)
            .afterOpened()
            .subscribe((): void => {
                QRCode.toCanvas(document.getElementById('canvas'), this.currentAddress, { scale: 6 } as QRCode.QRCodeRenderersOptions);
            }));
    }

    private setupForms(): void {
        this.sendForm = this.fb.group({
            toAddress: ['', Validators.required],
            amount: ['', Validators.required],
        });
    }
}
