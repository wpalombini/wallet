import { Component, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { slideInAnimation } from '../../animations/animations';
import { MatSidenav } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';

@Component({
    animations: [slideInAnimation],
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnDestroy {
    public mobileQuery: MediaQueryList;

    @ViewChild('snav')
    public sideNavPanel: MatSidenav;

    private mobileQueryListener: () => void;
    private subscription: Subscription = new Subscription();

    constructor(
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
    ) {
        this.mobileQuery = media.matchMedia('(max-width: 1330px)');
        this.mobileQueryListener = (): void => changeDetectorRef.detectChanges();
        // tslint:disable-next-line:deprecation
        this.mobileQuery.addListener(this.mobileQueryListener);
    }

    ngOnDestroy(): void {
        // tslint:disable-next-line:deprecation
        this.mobileQuery.removeListener(this.mobileQueryListener);
        this.subscription.unsubscribe();
    }

    public onLinkClicked(): void {
        if (this.mobileQuery.matches) {
            this.sideNavPanel.close();
        }
    }
}
